@echo off
rem Just a wrapper to run the matching PowerShell script.
pushd "%~dp0"
PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "& '.\upgrade.ps1' 2"
popd