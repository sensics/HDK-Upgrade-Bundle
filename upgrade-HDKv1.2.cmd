@echo off
rem Primarily a wrapper to run the matching PowerShell script.
pushd %~dp0
rem Start the driver force-installer in case this is a not-previously-updated 1.2
start /wait force-install\force-install-libwdi
rem The -AcceptZeroHdks flag should permit the script to deal with finding the old VID/PID.
PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "& '.\upgrade.ps1' 1.2 -AcceptZeroHdks"
popd
