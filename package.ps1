
[CmdletBinding()]
Param(
    [switch]$SelfExtractor,
    [switch]$Dummy
)

# Clean up hdktool logs.
Get-ChildItem * -Include "hdktool.log" -Recurse | Remove-Item
$outstem = 'HDK-Upgrade-Bundle-1-84-2-00'
if ($SelfExtractor) {
  $outfile = "$outstem.exe"
} else {
  $outfile = "$outstem.zip"
}

$sevenzip = '7za'
$sfx='7z.sfx'
$sevenzipbase=Join-Path $env:ChocolateyInstall "lib\7zip.commandline\tools"
if (("$env:ChocolateyInstall" -ne '') -and (Test-Path $sevenzipbase)) {
  $sevenzip=Join-Path $sevenzipbase '7z.exe'
  $sfx=Join-Path $sevenzipbase '7z.sfx'
  Write-Host 'Found Chocolatey install of 7zip.commandline: -SelfExtractor more likely to work!'
} else {
  Write-Host 'For best results, run "choco install -y 7zip.commandline" first - we could not find this.'
}

$DirName=$(Get-Item .).Name

$directories = @('dfu-programmer', 'drivers', 'hdktool', 'force-install')

$files = @(Get-Item 'upgrade.ps1') +
    @(Get-ChildItem *.cmd) +
    @(Get-ChildItem *.hex -Exclude hdk-firmware-read*)

$sources = $files + @($directories | ForEach-Object { Get-Item $_ })
$args = @('a', "$DirName\$outfile")

if ($SelfExtractor) {
  $args += "-sfx$sfx"
} else {
  $args += '-tzip'
}

Push-Location ..
  # Since we were getting "Item" objects, not path strings, we can do the last-minute directory change.
  # Need the trim start so 7z doesn't strip the leading directory.
  $args += @($sources | Resolve-Path -Relative | ForEach-Object { $_.TrimStart(".\") })
  if ($Dummy) {
    Write-Host "Arguments to $sevenzip :"
    Write-Host $args
  } else {
    Start-Process "$sevenzip" -ArgumentList $args -NoNewWindow -Wait
  }

Pop-Location
