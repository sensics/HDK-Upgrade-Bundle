# HDK Firmware Upgrade Script
#
# Copyright 2016-2017 Sensics, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True,Position=1)]
    [string]$hdkVer = '1.2',

    [switch]$AcceptZeroHdks,
    [switch]$SkipDrivers,
    [switch]$SkipFlash,
    [switch]$SkipHDMI,
    [string]$HexFile,
    [switch]$ReadInstead
)

$scriptRoot = Split-Path -parent $MyInvocation.MyCommand.Definition

$DriverInstallFilename = 'OSVR-HDK-Combined-Driver-Installer-1.2.8.exe'

$HASHALG = 'SHA256'

$FIRMWARE = @{
    "1.2" = "HMD-MCU-HDK-OLED-1-84.hex"
    "1.3" = "hdk_oled-HDK-1.3-1.4-2.00.hex"
    "1.3-wirelessonly" = "hdk_oled-WirelessOnly-HDK-1.3-1.4-2.00.hex"
    "2" = "hdk2svr-2.00.hex"
}

$HASHES = @{
    "HMD-MCU-HDK-OLED-1-84.hex" = "d2c3c55f5d49afad2aad70d56d8a2ac8fab1b731729b76c92e396994eeec9282"
    "hdk_oled-HDK-1.3-1.4-2.00.hex" = "e0f9e71ceafa2e5905a4f0cd44739253bca70c67fc4cd393393cd1aad192b7aa"
    "hdk_oled-WirelessOnly-HDK-1.3-1.4-2.00.hex" = "cb5040c3626a648bc5bbad06e794e64cee74db0da2971b884a01e6b7453297d9"
    "hdk2svr-2.00.hex" = "36c7eb20dec518c400b8cd8eda0189c9c1622be05c4fb87ced2359f9667593a0"
}

$MCU = 'atxmega256a3bu'


function Get-HashFile($fn, $algorithm) {
    $(CertUtil -hashfile $fn)[1] -replace ' ',''
}

function Check-HashFile($fn) {

    $expected = $HASHES[$fn]
    $computed = $(Get-HashFile($fn, $HASHALG))

    #Write-Host "Expected hash: $expected"

    #Write-Host "Computed hash: $computed"
    return ($computed -eq $expected)
}

function Get-NumHDKs() {
    return @(Get-WmiObject -class Win32_PnPEntity -namespace 'root\CIMV2' |
        where {$_.HardwareID -like 'USB\VID_1532&PID_0b00' -and $_.Service -eq 'usbccgp'}).Count
}

function Get-NumBootloaders() {
    return @(Get-WmiObject -class Win32_PnPEntity -namespace 'root\CIMV2' |
        where {$_.HardwareID -like 'USB\VID_03EB&PID_2FE2'}).Count
}

function Get-DriverStoreEnum() {
    $nl = [System.Environment]::NewLine
    ((pnputil.exe -e )| Out-String) -split "$nl$nl" | % {
        if ($_.ToString().Contains(':')) {
            # Only process chunks that have a property
            $tempHash = @{}
            $cleaned = ($_ `
                -replace 'Published name','PublishedName' `
                -replace 'Driver package provider', 'DriverPackageProvider' `
                -replace 'Driver date and version', 'DriverDateAndVersion' `
                -replace 'Signer name', 'SignerName')
            # Split cleaned input into array of lines
            $cleaned -split "$nl" -split '`n' | % {
                # split each line on its first :
                $parts = $_ -split ':',2
                # add key and value to hash
                $tempHash.Add($parts[0].Trim(), $parts[1].Trim())
            }
            $object = New-Object �TypeName PSObject �Prop $tempHash
            Write-Output $object
        }
    }
}

function Get-HasRequiredDrivers() {
    Write-Host 'Checking for required drivers...'
    $infDir = Join-Path $env:SystemRoot 'inf'
    $driverStore = Get-DriverStoreEnum

    # Look for DFU driver inf.
    $numDfuDrivers = @($driverStore | Where-Object  {$_.DriverPackageProvider.Contains('Atmel')} |
        ForEach-Object { Get-Item (Join-Path $infDir $_.PublishedName) } |
        Where-Object { Select-String -Path $_ -Pattern 'USB\VID_03EB&PID_2FE2' -SimpleMatch -Quiet }).Count
    # Look for an HDK driver inf.
    $numHDKDrivers = @($driverStore | Where-Object  {$_.DriverPackageProvider.Contains('Sensics')} |
        ForEach-Object { Get-Item (Join-Path $infDir $_.PublishedName) } |
        Where-Object { Select-String -Path $_ -Pattern 'USB\VID_1532&PID_0B00' -SimpleMatch -Quiet }).Count
    return (($numDfuDrivers -gt 0) -and ($numHDKDrivers -gt 0))
}


function Enter-Bootloader() {
    Write-Host 'Putting HMD into Bootloader mode...'
    $Proc = Start-Process -FilePath (Join-Path $scriptRoot 'hdktool\enter-bootloader.exe') -NoNewWindow -Wait -PassThru
    if ($Proc.ExitCode -ne 0) {
        Write-Error "enter-bootloader exited with non-zero error code. You may need to unplug/replug the device."
        exit 1
    }
    While ($(Get-NumBootloaders) -eq 0) {
        Write-Host 'Waiting for DFU driver install... if this goes on a long time, cancel, unplug/replug, and try again.'
        Start-Sleep 1
    }
}

function Show-FWVersion() {
    Start-Process -FilePath (Join-Path $scriptRoot 'hdktool\get-firmware-version.exe') -ArgumentList @('--scripted') -NoNewWindow -Wait
}

function Update-EDID() {
    Write-Host 'Will update HMD EDID data - for best results, connect HDMI now and ensure direct mode is disabled.'
    Read-Host 'Press the enter key to proceed.'

    Write-Host 'Updating HMD EDID data...'
    $Proc = Start-Process -FilePath (Join-Path $scriptRoot 'hdktool\hdmi-update.exe') -NoNewWindow -Wait
    if ($Proc.ExitCode -ne 0) {
        Write-Error "hdmi-update exited with non-zero error code. You may need to unplug/replug the device, plug in HDMI and make sure video is being displayed, and re-run hdmi-update."
        exit 1
    }
}

function DFU-Programmer
{
    Param(
        [String[]]$ArgList,
        [String]$OutFile = $null
    )
    if ($ArgList.Contains($MCU)) {
        $GoodArgs = $ArgList
    } else {
        $GoodArgs = @($MCU) + $ArgList
    }
    $ProgrammerApp = Get-Item (Join-Path $scriptRoot 'dfu-programmer\dfu-programmer.exe')
    Write-Debug "dfu-programmer arguments: $GoodArgs"
    if ($OutFile -ne $null -and -not ($OutFile.Length -eq 0) ) {
        Write-Debug "Redirecting standard output to $OutFile"
        Start-Process -FilePath $ProgrammerApp -ArgumentList $GoodArgs -Wait -NoNewWindow -RedirectStandardOutput "$OutFile"
    } else {
        Start-Process -FilePath $ProgrammerApp -ArgumentList $GoodArgs -Wait -NoNewWindow
    }
}

function Write-Firmware($fn) {
    $item = Get-Item "$fn"
    Write-Host "Writing new firmware file $($item.FullName) to device... (Note: You can generally ignore 'validation failure' messages)"
    DFU-Programmer @('flash', '--force', '--suppress-bootloader-mem', """$($item.FullName)""")
}

function Do-ReadFirmware {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$True, Position=0)]
        [ValidateSet('eeprom', 'flash')]
        [String]$Memory,
        [Parameter(Mandatory=$True, Position=1)]
        [String]$FileStem,
        [Switch]$bin
    )
    if ($bin) {
        $filetype = 'binary'
        $ext = 'bin'
        $filetypeArgs = @('--bin')
    } else {
        $filetype = 'ihex'
        $ext = 'hex'
        $filetypeArgs = @()
    }
    switch ($Memory) {
        'flash' {
            $sectionName = 'program flash'
            $sectionArgs = @()
        }
        'eeprom' {
            $sectionName = 'data EEPROM'
            $sectionArgs = @('--eeprom')
        }
    }

    $outFile = "$FileStem-$Memory.$ext"
    $allArgs = @('read') + $filetypeArgs + $sectionArgs
    Write-Verbose "Reading $sectionName memory from device to $filetype file $outFile"
    DFU-Programmer @($allArgs) -OutFile "$outFile"
}

function Read-Firmware() {
    $fileStem = Join-Path $scriptRoot "hdk-firmware-read"
    Do-ReadFirmware flash -FileStem $fileStem
    Do-ReadFirmware flash -FileStem $fileStem -bin

    Do-ReadFirmware eeprom -FileStem $fileStem
    Do-ReadFirmware eeprom -FileStem $fileStem -bin
}

function Do-RestartHDK() {
    Write-Host "Restarting HMD to exit bootloader mode..."

    DFU-Programmer @('launch')
}


###
# Main entry point
###

if ($hdkVer -eq "2" -or $hdkVer -eq "2-dev") {
    Write-Verbose "HDK2 implies SkipHDMI since HDMI update command not applicable for HDK2-based devices"
    $SkipHDMI = $True
}

if ($ReadInstead) {
    Write-Host "Firmware and EEPROM read from HDK requested."
} else {
    if ("$HexFile".Length -ne 0) {
        Write-Host "Firmware update for HDK requested, hex file provided: $HexFile"
        $fn = (Get-Item "$HexFile").FullName
    } else {
        Write-Host "Firmware update for HDK v$hdkVer requested."
        $fn = $FIRMWARE[$hdkVer]

        # TODO fix hash check!
        #if ($(Check-Hash $fn) -eq $false) {
        #    Write-Host "ERROR: firmware failed a hash check - this download is corrupted! Please get another copy."
        #    exit 1
        #}
    }
    Write-Host "Firmware file $fn will be used."
    if ($fn -eq $null -or -not (Test-Path "$fn")) {
        Write-Error "Could not find $fn - exiting."
        exit 1
    }
}

Write-Host "Ensure the desired device is connected, and all other HDKs, other Sensics HMDs, and Atmel XMEGA microcontrollers are disconnected."
Read-Host 'Press the enter key to proceed.'

if (-not $SkipDrivers) {
  if (-not $(Get-HasRequiredDrivers)) {
      Write-Host 'Installing OSVR HDK and bootloader drivers...'
      Start-Process (Join-Path $PSScriptRoot "drivers\$DriverInstallFilename") -ArgumentList @('/S') -Wait # can't add NoNewWindow or it can't elevate.
  }
}

if (-not $SkipFlash) {
  $NumBootloaders = Get-NumBootloaders
  if ($(Get-NumBootloaders) -eq 0) {
    $NumHDK = Get-NumHDKs
    if (($NumHDK -gt 1) -or ($NumHDK -eq 0 -and -not $AcceptZeroHdks)) {
        Write-Host "Must have exactly 1 HDK connected - detected $NumHDK"
        Read-Host 'Press the enter key to exit'
        exit 1
    }

    Enter-Bootloader
  } else {
    Write-Host "Found device already in bootloader mode!"
  }
  if ($ReadInstead) {
    Read-Firmware
  } else {
    Write-Firmware $fn
  }
  Do-RestartHDK
}

While ($(Get-NumHDKs) -eq 0) {
    Write-Host 'Waiting for HDK reboot to finish... if this goes on a long time, unplug and replug the device.'
    Start-Sleep 1
}

if (-not $SkipHDMI -and -not $ReadInstead) {
	Update-EDID
}

Show-FWVersion

Write-Host 'Process complete! You may want to power-cycle/unplug and replug the device for best results.'
Read-Host 'Press the enter key to exit'
