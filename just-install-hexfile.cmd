rem @echo off
rem Just a wrapper to run the matching PowerShell script.
PowerShell.exe -NoProfile -NoLogo -ExecutionPolicy Bypass -Command "& '%~dp0\upgrade.ps1' 1.3 -Verbose -HexFile '%1'  -SkipDrivers -SkipHDMI"
